<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text" version="1.0" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

  <xsl:include href="Common.xslt"/>

  <xsl:template name="code-gen">
    <xsl:apply-templates select="Root"/>
  </xsl:template>

  <xsl:template match="Root">

    using System.Drawing;
    using System.Windows.Forms;

    partial class <xsl:value-of select="$classname"/>Form: Form
    {

      <xsl:apply-templates select="Control" mode="declare" />

      public <xsl:value-of select="$classname"/>Form()
      {
         LoadControls();
      }

      void LoadControls()
      {
        <xsl:apply-templates select="Control" mode="create" />
        <xsl:apply-templates select="Control" mode="set-props" />
        <xsl:apply-templates select="Control" mode="add-to-form" />
      }

    }

  </xsl:template>

  <xsl:template match="Control" mode="declare">
    private <xsl:value-of select="@type"/> _<xsl:value-of select="@var-name"/>;
  </xsl:template>

  <xsl:template match="Control" mode="create">
    _<xsl:value-of select="@var-name"/> = new <xsl:value-of select="@type"/>();
  </xsl:template>

  <xsl:template match="Control" mode="set-props">
    <xsl:apply-templates select="@*" mode="set-props">
      <xsl:with-param name="var-name" select="@var-name"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="Control" mode="add-to-form">
    Controls.Add( _<xsl:value-of select="@var-name"/> );
  </xsl:template>

  <xsl:template match="@*" mode="set-props">
    <xsl:param name="var-name">???</xsl:param>
    <xsl:if test="name()!='type' and name()!='var-name'">
      _<xsl:value-of select="$var-name"/>.<xsl:value-of select="name()"/> = <xsl:value-of select="."/>;
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
