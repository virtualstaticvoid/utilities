
    #region Generated File
    /*
     * GENERATED FILE -- DO NOT EDIT
     *
     * Generator: TransformCodeGenerator, Version=9.0.3216.20563, Culture=neutral, PublicKeyToken=null
     * Version:   9.0.3216.20563
     *
     * Generated code from "Products.xml"
     *
     * Created: Tuesday, October 21, 2008
     * By:      XYZ\Abc
     *
     */
    #endregion
  
    namespace TestXSLTCodeGenerator
    {
      

    using System;
    using System.Data;

    partial class ProductsDataTable: DataTable
    {

      public ProductsDataTable()
      {
        InitColumns();
      }

      public ProductsDataTable(string tableName):
        base(tableName)
      {
        InitColumns();
      }
      
      void InitColumns()
      {
        
     Columns.Add(new DataColumn("Id", typeof(Int32)));
  
     Columns.Add(new DataColumn("Code", typeof(String)));
  
     Columns.Add(new DataColumn("Description", typeof(String)));
  
      }

      public new ProductsDataRow NewRow()
      {
        return (ProductsDataRow)base.NewRow();
      }

      protected override DataTable CreateInstance()
      {
        return new ProductsDataTable();
      }

      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return new ProductsDataRow(builder);
      }

  }

  partial class ProductsDataRow: DataRow
    {

      public ProductsDataRow(DataRowBuilder builder):
        base(builder)
      {
      }

      
     public Int32 Id
    {
      get { return (Int32)this["Id"]; }
      set { this["Id"] = value; }
    }
  
     public String Code
    {
      get { return (String)this["Code"]; }
      set { this["Code"] = value; }
    }
  
     public String Description
    {
      get { return (String)this["Description"]; }
      set { this["Description"] = value; }
    }
  

    }

  
    }
  