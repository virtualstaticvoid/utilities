<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text" version="1.0" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

  <xsl:include href="Common.xslt"/>

  <xsl:template name="code-gen">
    <xsl:apply-templates select="DataTable"/>
  </xsl:template>
  
  <xsl:template match="DataTable">

    using System;
    using System.Data;

    partial class <xsl:value-of select="$classname"/>DataTable: DataTable
    {

      public <xsl:value-of select="$classname"/>DataTable()
      {
        InitColumns();
      }

      public <xsl:value-of select="$classname"/>DataTable(string tableName):
        base(tableName)
      {
        InitColumns();
      }
      
      void InitColumns()
      {
        <xsl:apply-templates select="Fields/Field" mode="init"/>
      }

      public new <xsl:value-of select="$classname"/>DataRow NewRow()
      {
        return (<xsl:value-of select="$classname"/>DataRow)base.NewRow();
      }

      protected override DataTable CreateInstance()
      {
        return new <xsl:value-of select="$classname"/>DataTable();
      }

      protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
      {
        return new <xsl:value-of select="$classname"/>DataRow(builder);
      }

  }

  partial class <xsl:value-of select="$classname"/>DataRow: DataRow
    {

      public <xsl:value-of select="$classname"/>DataRow(DataRowBuilder builder):
        base(builder)
      {
      }

      <xsl:apply-templates select="Fields/Field" mode="props"/>

    }

  </xsl:template>

  <xsl:template match="Field" mode="init">
     Columns.Add(new DataColumn("<xsl:value-of select="@name"/>", typeof(<xsl:value-of select="@type"/>)));
  </xsl:template>

  <xsl:template match="Field" mode="props">
     public <xsl:value-of select="@type"/> <xsl:text>&#32;</xsl:text> <xsl:value-of select="@name"/>
    {
      get { return (<xsl:value-of select="@type"/>)this["<xsl:value-of select="@name"/>"]; }
      set { this["<xsl:value-of select="@name"/>"] = value; }
    }
  </xsl:template>

</xsl:stylesheet>
