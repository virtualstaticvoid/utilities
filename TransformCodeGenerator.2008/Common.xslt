<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text" version="1.0" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

  <!-- parameters passed in by the TransformCodeGenerator -->
  <xsl:param name="generator"></xsl:param>
  <xsl:param name="version"></xsl:param>
  <xsl:param name="fullfilename"></xsl:param>
  <xsl:param name="filename"></xsl:param>
  <xsl:param name="date-created"></xsl:param>
  <xsl:param name="created-by"></xsl:param>
  <xsl:param name="namespace"></xsl:param>
  <xsl:param name="classname"></xsl:param>

  <xsl:template match="/">
    <xsl:call-template name="header-comment"/>
    namespace <xsl:value-of select="$namespace"/>
    {
      <xsl:call-template name="code-gen"/>
    }
  </xsl:template>

  <xsl:template name="header-comment">
    #region Generated File
    /*
     * GENERATED FILE -- DO NOT EDIT
     *
     * Generator: <xsl:value-of select="$generator"/>
     * Version:   <xsl:value-of select="$version"/>
     *
     * Generated code from "<xsl:value-of select="$filename"/>"
     *
     * Created: <xsl:value-of select="$date-created"/>
     * By:      <xsl:value-of select="$created-by"/>
     *
     */
    #endregion
  </xsl:template>

</xsl:stylesheet>
