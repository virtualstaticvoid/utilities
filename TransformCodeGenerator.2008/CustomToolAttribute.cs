namespace TransformCodeGenerator
{
  using System;

  [AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
  public class CustomToolAttribute: Attribute
  {

    public CustomToolAttribute(string name):
      this(name, String.Empty)
    {
    }

    public CustomToolAttribute(string name, string description)
    {
      this.Name = name;
      this.Description = description;
    }

    public string Name { get; private set; }

    public string Description { get; private set; }

  }
}
