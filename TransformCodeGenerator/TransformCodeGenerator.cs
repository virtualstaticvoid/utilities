/*
 * Copyright (C) 2006 Chris Stefano
 *       cnjs@mweb.co.za
 */
namespace TransformCodeGenerator
{
  using System;
  using System.IO;
  using System.Reflection;
  using System.Runtime.InteropServices;
  using System.Xml;
  using System.Xml.XPath;
  using System.Xml.Xsl;

  /// <summary>
  ///
  /// </summary>
  [Guid("07834038-5EA7-4d0d-8194-B8E91DC75638")]
  [CustomTool("TransformCodeGenerator", "Transform Code Generator")]
  public class TransformCodeGenerator: CustomToolBase
  {

    /// <summary>
    /// 
    /// </summary>
    public TransformCodeGenerator()
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inputFileName"></param>
    /// <param name="inputFileContent"></param>
    /// <returns></returns>
    public override string DoGenerateCode(string inputFileName, string inputFileContent)
    {

      string transformerFileName = "NOT FOUND";
      StringWriter outputWriter = new StringWriter();
      try
      {

        FileInfo inputFileInfo = new FileInfo(inputFileName);

        // get the source document
        XmlDocument sourceDocument = new XmlDocument();
        sourceDocument.LoadXml(inputFileContent);

        // get the filename of the transformer
        string transformerFile = sourceDocument.DocumentElement.Attributes["transformer"].Value;
        
        if(!File.Exists(transformerFileName))
        {
          // try in the same dir as the file
          transformerFileName = Path.Combine(inputFileInfo.DirectoryName, transformerFile);

          if (!File.Exists(transformerFileName))
          {
            // try in the dir where this dll lives
            FileInfo assemblyFileInfo = new FileInfo(Assembly.GetExecutingAssembly().Location);
            transformerFileName = Path.Combine(assemblyFileInfo.DirectoryName, transformerFile);
          }
        }

        // get the xslt document
        XPathDocument transformerDoc = new XPathDocument(transformerFileName);

        // create the transform
        XslCompiledTransform xslTransform = new XslCompiledTransform();
        xslTransform.Load(transformerDoc.CreateNavigator(), XsltSettings.Default, null);

        FileInfo fi = new FileInfo(inputFileName);

        XsltArgumentList args = new XsltArgumentList();

        AssemblyName assemblyName = Assembly.GetExecutingAssembly().GetName();

        args.AddParam("generator", String.Empty, assemblyName.FullName);
        args.AddParam("version", String.Empty, assemblyName.Version.ToString());
        args.AddParam("fullfilename", String.Empty, inputFileName);
        args.AddParam("filename", String.Empty, fi.Name);
        args.AddParam("date-created", String.Empty, DateTime.Today.ToLongDateString());
        args.AddParam("created-by", String.Empty, String.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName));
        args.AddParam("namespace", String.Empty, base.FileNameSpace);
        args.AddParam("classname", String.Empty, fi.Name.Substring(0, fi.Name.LastIndexOf(".")));

        // do the transform
        xslTransform.Transform(sourceDocument, args, outputWriter);

      }
      catch(Exception ex)
      {
        outputWriter.WriteLine("/*");
        outputWriter.WriteLine("\tERROR: Unable to generate output for template:");
        outputWriter.WriteLine("\t'{0}'", inputFileName);
        outputWriter.WriteLine("\tUsing transformer:");
        outputWriter.WriteLine("\t'{0}'", transformerFileName);
        outputWriter.WriteLine("");
        outputWriter.WriteLine(ex.ToString());
        outputWriter.WriteLine("*/");
      }

      return outputWriter.ToString();

    }

  }

}
