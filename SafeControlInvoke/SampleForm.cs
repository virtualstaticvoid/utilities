/*
 *           VirtualStaticVoid
 *  http://dotnet.org.za/virtualstaticvoid
 *  
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Patterns
{
  public partial class SampleForm : Form
  {
    public SampleForm()
    {
      InitializeComponent();
    }

    private void buttonPopulate_Click(object sender, EventArgs e)
    {
      this.backgroundWorker.RunWorkerAsync();
    }

    private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
    {

      this.buttonPopulate.SafeControlInvoke(button => button.Enabled = false);

      #region Simulate long running task

      List<ListViewItem> listViewItems = new List<ListViewItem>();

      for (int i = 0; i < 10000; i++)
      {
        listViewItems.Add(new ListViewItem(i.ToString()));
      }

      #endregion

      this.listViewDisplay.SafeControlInvoke
        (
          listView =>
            {
              listView.BeginUpdate();
              listView.Items.Clear();
              listView.Items.AddRange(listViewItems.ToArray());
              listView.EndUpdate();
            }
        );

      this.buttonPopulate.SafeControlInvoke(button => button.Enabled = true);

    }
  }
}
