/*
 *           VirtualStaticVoid
 *  http://dotnet.org.za/virtualstaticvoid
 *  
 */
namespace Patterns
{
  partial class SampleForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.buttonPopulate = new System.Windows.Forms.Button();
      this.listViewDisplay = new System.Windows.Forms.ListView();
      this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
      this.SuspendLayout();
      // 
      // buttonPopulate
      // 
      this.buttonPopulate.Location = new System.Drawing.Point(8, 8);
      this.buttonPopulate.Name = "buttonPopulate";
      this.buttonPopulate.Size = new System.Drawing.Size(116, 32);
      this.buttonPopulate.TabIndex = 0;
      this.buttonPopulate.Text = "Populate Async";
      this.buttonPopulate.UseVisualStyleBackColor = true;
      this.buttonPopulate.Click += new System.EventHandler(this.buttonPopulate_Click);
      // 
      // listViewDisplay
      // 
      this.listViewDisplay.Location = new System.Drawing.Point(8, 48);
      this.listViewDisplay.Name = "listViewDisplay";
      this.listViewDisplay.Size = new System.Drawing.Size(280, 212);
      this.listViewDisplay.TabIndex = 1;
      this.listViewDisplay.UseCompatibleStateImageBehavior = false;
      // 
      // backgroundWorker
      // 
      this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
      // 
      // SampleForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(297, 269);
      this.Controls.Add(this.listViewDisplay);
      this.Controls.Add(this.buttonPopulate);
      this.Name = "SampleForm";
      this.Text = "SafeControlInvoke";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button buttonPopulate;
    private System.Windows.Forms.ListView listViewDisplay;
    private System.ComponentModel.BackgroundWorker backgroundWorker;
  }
}