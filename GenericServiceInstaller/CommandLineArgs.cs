/*
 * Copyright (C) 2007 
 *  Written by C.Stefano
 */
namespace GenericServiceInstaller
{
  using System;
  using System.Text;
  using System.IO;

  class CommandLineArgs: CommandLineParser
  {

    [CommandLineParser.NoUsage()]
    private string _name = String.Empty;

    [CommandLineParser.NoUsage()]
    private string _displayName = String.Empty;

    [CommandLineParser.NoUsage()]
    private string _application = String.Empty;

    [CommandLineParser.NoUsage()]
    private string _applicationArgs = String.Empty;

    [CommandLineParser.NoUsage()]
    private bool _uninstall = false;

    [CommandLineParser.NoUsage()]
    private bool _debug = false;

    public CommandLineArgs()
    {
    }

    [CommandLineParser.ValueUsage("Name of the service", Name = "name", Optional = false)]
    public string Name
    {
      get
      {
        return this._name;
      }
      set
      {
        this._name = value;
      }
    }

    [CommandLineParser.ValueUsage("Display name of the service", Name = "display", Optional = true)]
    public string DisplayName
    {
      get
      {
        return this._displayName;
      }
      set
      {
        this._displayName = value;
      }
    }

    [CommandLineParser.ValueUsage("The fully qualified path to the service application.", Name = "app", Optional = true)]
    public string Application
    {
      get
      {
        return this._application;
      }
      set
      {
        this._application = value;
      }
    }

    [CommandLineParser.ValueUsage("Command line arguments for the service application. Note: Use an apostrophe for quotes when nested quotes are required.", Name = "args", Optional = true)]
    public string ApplicationArgs
    {
      get
      {
        return this._applicationArgs.Replace("'", "\"");
      }
      set
      {
        this._applicationArgs = value;
      }
    }

    [CommandLineParser.FlagUsage("Uninstall", Name = "uninstall", AlternateName1 = "u", Optional = true)]
    public bool Uninstall
    {
      get
      {
        return this._uninstall;
      }
      set
      {
        this._uninstall = value;
      }
    }

    [CommandLineParser.FlagUsage("Debug", Name = "debug", AlternateName1 = "d", Optional = true)]
    public bool Debug
    {
      get
      {
        return this._debug;
      }
      set
      {
        this._debug = value;
      }
    }

  }
}
