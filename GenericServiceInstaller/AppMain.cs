/*
 * Copyright (C) 2007 
 *  Written by C.Stefano
 */
namespace GenericServiceInstaller
{
  using System;

  /// <summary>
  /// Summary description for AppMain.
  /// </summary>
  class AppMain
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static int Main(string[] args)
    {
      // create class to hold command line arguments
      CommandLineArgs commandLineArgs = new CommandLineArgs();

      // parse the command line
      if (!commandLineArgs.ParseAndContinue(args))
      {
        // return failure
        return 1; // error-level 1
      }

      // write out the logo
      Console.Write(commandLineArgs.GetLogo());
      Console.WriteLine();

      // launch the debugger?
      if (commandLineArgs.Debug)
        System.Diagnostics.Debugger.Launch();   // launch and attach a debugger

      bool success;

      if(commandLineArgs.Uninstall)
      {
        success = ServiceInstaller.UnInstallService(commandLineArgs.Name);
      }
      else
      {
        string applicationCmd = commandLineArgs.Application;
        if(commandLineArgs.ApplicationArgs!=String.Empty)
          applicationCmd += " " + commandLineArgs.ApplicationArgs;

        success = ServiceInstaller.InstallService
          (
            commandLineArgs.Name,
            commandLineArgs.DisplayName,
            applicationCmd
          );
      }

      Console.WriteLine();
      Console.WriteLine
        (
          "{0} {1} {2} service.",
          /* 0 */ (success ? "Successfully" : "ERROR: Failed to"),
          /* 1 */ (commandLineArgs.Uninstall ? "uninstalled" : "installed"),
          /* 2 */ commandLineArgs.Name
        );

      // return success
      return success ? 0 : 1;

    }
  }
}
